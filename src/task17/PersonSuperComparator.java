package task17;

class PersonSuperComparator implements Comparator{
    String name;
    int age;
    PersonSuperComparator(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int comparison(Comparator two) {
        if(two.age  == age) {
            return name.compareTo(two.name);
        }
        return age-two.age;
    }

    @Override
    public int comparison(String name, int age) {
        if(age  == this.age) {
            return this.name.compareTo(name);
        }
        return this.age-age;
    }
}
