package task13;

import java.io.*;
import java.nio.charset.Charset;

public class Encoding {


    public static void main(String[] args) {
        try (FileInputStream fis = new FileInputStream(args[0]);
             FileOutputStream fos = new FileOutputStream(args[1]);
             Reader reader = new InputStreamReader(fis, Charset.forName("Windows-1252"));
             Writer writer  = new OutputStreamWriter(fos,Charset.forName("UTF-8"));){

            int c;
            while ((c = reader.read()) != -1) {


                    writer.write((char) c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}