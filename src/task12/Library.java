package task12;
import java.util.*;
import java.io.*;

class library {
    private List<Book> collection = new ArrayList<>();
    private String fileName;

    library (String filePathString) throws IOException {
        fileName = filePathString;
        File f = new File(filePathString);
        if(!f.exists())
            return;


        FileReader reader = new FileReader(f);
        Scanner file = new Scanner(reader);

        while(file.hasNextLine()) {
            Book buf = new Book();

            buf.name = file.nextLine();
            buf.author = file.nextLine();
            buf.dateCreate = file.nextLine();

            collection.add(buf);
        }

        reader.close();
    }

    private void save(Book book) throws IOException {
        FileWriter writer = new FileWriter(fileName, true);
        writer.write(book.name + "\n" + book.author + "\n" + book.dateCreate + "\n");
        writer.close();
    }

    public void addBook() throws IOException {
        Scanner in = new Scanner(System.in);
        Book temp = new Book();

        System.out.println("Введите название");
        temp.name = in.nextLine();
        System.out.println("Введите имя автора");
        temp.author = in.nextLine();
        System.out.println("Введите год издания");
        temp.dateCreate = in.nextLine();

        collection.add(temp);
        save(temp);
    }

    public void out() {
        for (Book temp: collection)
            temp.out();
    }
}