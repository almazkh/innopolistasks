package task12;
import java.util.*;
import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {

        library library = new library("library.txt");
        Scanner in = new Scanner(System.in);

        while(true)
        {
            System.out.println("1.Добавить\n2.Вывести");
            int select = in.nextInt();
            switch (select)
            {
                case 1:
                    library.addBook();
                    break;
                case 2:
                    library.out();
                    break;
                default:
                    System.out.println("Error");
            }
        }

        // write your code here
    }
}