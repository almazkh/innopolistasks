package ru.hammatshin.task2;
import java.util.Scanner;
public class HotCold {
        public static void main(String[] args){
            int a,b,c=0;
            //Генерируем случайное целое число из диапазона 1,100
            //Если a=0 повторяем генерацию
            do{
                a= (int)(Math.random()*100-1);
            }
            while(a==0);
            Scanner scn=new Scanner(System.in);
            System.out.println("Введите целое число из отрезка 1,100");
            if(scn.hasNextInt()){
                do{
                    b=scn.nextInt();
                    if(b < 1 || b > 100){
                        System.out.println("Вы ввели число не из-заданного диапазона или 0");
                        System.out.println("Повторите ввод");
                    }
                    else{
                        ++c;
                        if(b==a){
                            System.out.println("Вы угадали с "+c+" попытки");
                        }
                        else{
                            if(b<a)System.out.println("Число горячо");
                            else System.out.println("Число холодно");
                            if(a < 0)
                                System.out.println("Число с другим знаком");
                        }
                    }
                }

                while(b!=a);
            }
            else System.out.println("Ошибка. Введено не целое число");
        }
}
