package ru.hammatshin.task5;

public class Drink {
    public static enum DrinkType {
       Coca,Fanta,Sprite,Tea,Water
    }
    DrinkType name;
    double price;
    public Drink(DrinkType name, double price){
       this.name=name;
       this.price=price;
    }
    public String toString() {
       String result;
       result = name+" "+price;
       return result;
    }
}
