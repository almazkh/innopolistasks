package ru.hammatshin.task4;

import java.time.LocalDate;

public class Seller extends Person{
    private int reportCardNumber;

    public Seller(String firstName, String lastName, LocalDate birthday) {
        super(firstName, lastName, birthday);
    }

    public int getReportCardNumber() {
        return reportCardNumber;
    }

    public void setReportCardNumber(int reportCardNumber) {
        this.reportCardNumber = reportCardNumber;
    }
}

