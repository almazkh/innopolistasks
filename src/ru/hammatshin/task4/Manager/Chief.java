package ru.hammatshin.task4.Manager;

import ru.hammatshin.task4.Person;

import java.time.LocalDate;

public class Chief extends Person {
    private int salary;

    public Chief(String firstName, String lastName, LocalDate birthday) {
        super(firstName, lastName, birthday);
    }

    int getSalary() {
        return salary;
    }

     void setSalary(int salary) {
        this.salary = salary;
    }
}
