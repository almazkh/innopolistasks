package ru.hammatshin.task4.Manager;

import ru.hammatshin.task4.Person;

import java.time.LocalDate;

public class Accountant extends Person {
    public Accountant(String firstName, String lastName, LocalDate birthday) {
        super(firstName, lastName, birthday);
    }
}
