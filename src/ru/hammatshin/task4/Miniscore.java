package ru.hammatshin.task4;

import java.util.Arrays;

public class Miniscore {
    private String title;
    private Department[] depertments = new Department[7];

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Department[] getDepertments() {
        return depertments;
    }

    @Override
    public String toString() {
        return "Miniscore{" +
                "title='" + title + '\'' +
                ", depertments=" + Arrays.toString(depertments) +
                '}';
    }

    public void setDepertments(Department[] depertments) {
        this.depertments = depertments;

    }
}