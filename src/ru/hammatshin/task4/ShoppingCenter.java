package ru.hammatshin.task4;

import java.time.LocalDate;

public class ShoppingCenter {
    public static void main(String[] args){
    Seller maria = new Seller("Maria","Lavrova", LocalDate.now());

    maria.setReportCardNumber(8);

    Miniscore m15 = new Miniscore();
    Department d8 = new Department();

    d8.setSellers(new Seller[]{maria});

    m15.setDepertments(new Department[]{d8});

    System.out.println(m15);
    }
}