package ru.hammatshin.task4;

import java.util.Arrays;

public class Department{
    private String title;
    private Seller[] sellers = new Seller[450];

    public String getTitle() {
        return title;
    }

    public Seller[] getSellers() {
        return sellers;
    }

    public void setSellers(Seller[] sellers) {
        this.sellers = sellers;
    }

    @Override
    public String toString() {
        return "Department{" +
                "title='" + title + '\'' +
                ", sellers=" + Arrays.toString(sellers) +
                '}';
    }

    public void setTitle(String title) {
        this.title = title;

    }
}
