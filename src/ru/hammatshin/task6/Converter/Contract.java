package ru.hammatshin.task6.Converter;

import java.time.LocalDate;

public class Contract {
    private String title;
    private int number;
    private LocalDate date;
    private String product;

    public Contract(String title) {
        this.title = title;
        this.number = number;
        this.date = date;
        this.product = product;
    }

    public String getTitle() {
        return title;
    }

    public int getNumber() {
        return number;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getProduct() {
        return product;
    }
}

