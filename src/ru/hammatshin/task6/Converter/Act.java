package ru.hammatshin.task6.Converter;

import java.time.LocalDate;

public class Act {

    public static void main(String[] args) {
    }
    private String title;
    private int number;
    private LocalDate date;
    private String product;

    public Act(String title,int number,LocalDate date,String product){
        this.title = title;
        this.number = number;
        this.date = date;
        this.product = product;

    }

    public String getTitle() {

        return title;
    }

    public int getNumber() {
        return number;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getProduct() {
        return product;
    }

    public Act(String title) {
        this.title = title;
    }

    public static Act convert(Contract ct){
        Act aux = new Act(ct.getTitle());
        return aux;
    }
}




