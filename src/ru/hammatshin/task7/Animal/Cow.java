package ru.hammatshin.task7.Animal;

import ru.hammatshin.task7.Animal.Animal;

public class Cow extends Animal implements Run,Swim{
    @Override
    public void getNameAnimal() {
        System.out.println("Корова");


    }


    @Override
    public void doRun() {
        System.out.println("Корова бежит");
    }

    @Override
    public void doSwim() {
        System.out.println("Корова плавает");

    }
}