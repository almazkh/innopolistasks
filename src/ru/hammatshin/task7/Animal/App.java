package ru.hammatshin.task7.Animal;

public class App {
    public static void main(String[] args) {

        Swim cow = new Cow();
        Run cow2 = new Cow();
        Animal cow3 = new Cow();
        Wolf wolf = new Wolf();
        Owl owl = new Owl();

        cow3.getNameAnimal();
        owl.getNameAnimal();
        wolf.getNameAnimal();

        cow.doSwim();
        cow2.doRun();
        wolf.doRun();
        wolf.doSwim();
        owl.doFly();

        }
    }


