package ru.hammatshin.task7.Animal;

import ru.hammatshin.task7.Animal.Animal;

public class Wolf extends Animal implements Run,Swim {
    @Override
    public void getNameAnimal() {
        System.out.println("Волк");
    }

    @Override
    public void doRun() {
        System.out.println("Бежит по лесу");
    }

    @Override
    public void doSwim() {
        System.out.println("Волк плавает");
    }
}

