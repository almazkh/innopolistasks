package ru.hammatshin.task3;

import java.util.Scanner;

public class TwoWork {
    public static void main(String[] args) {
        int a = 0;
        System.out.println("Введите целое число");
        Scanner scn = new Scanner(System.in);
        if (scn.hasNextInt()) {
            do {
                a = scn.nextInt();
                String s  = ("Число ");
                if (a % 2 == 0) {
                    s = s + ("Четное ");
                } else {
                    s = s + ("Нечетеное ");
                }
                if (a >= 0) {
                    s = s + ("Положительное!");
                } else {
                    s = s + ("Отрицательное!");
                }
                System.out.println(s);
            } while (a != 0);
        } else System.out.println("Ошибка. Введено не целое число");
    }
}



