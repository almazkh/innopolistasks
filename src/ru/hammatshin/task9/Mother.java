package ru.hammatshin.task9;
import java.util.Scanner;
import java.util.Vector;

public class Mother {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Child child = new Child();

        int size = 0;

        for(food dir : food.values()) {
            System.out.println(dir.ordinal() + "." + dir);
            size = dir.ordinal();
        }
        int ind;
        while(true) {
            if (!sc.hasNextInt()) {
                System.out.println("Не корректный ввод, попробуйте снова");
                sc.nextLine();
                continue;
            }
            ind = sc.nextInt();
            if(size < ind) {
                System.out.println("Не корректный ввод, попробуйте снова");
                continue;
            }
            try {
                child.eat(food.values()[ind]);
                System.out.print("съел ");
                System.out.print(food.values()[ind]);
                System.out.println(" за обе щеки");
            } catch (Exception ex) {

            } finally {
                System.out.println("спасибо, мама");
            }
        }
    }
}