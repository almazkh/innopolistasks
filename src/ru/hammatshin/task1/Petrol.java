package ru.hammatshin.task1;

public class Petrol {
    public static void main(String[] args) {
        System.out.println("Hello, Petrol!");
        int pricePetrol = 43;
        int amountPetrol = 50;
        int sum;
        if (pricePetrol < amountPetrol) {
            sum = pricePetrol * amountPetrol;
        } else {
            sum = pricePetrol / amountPetrol;
        }
        System.out.println(sum);
    }
}
