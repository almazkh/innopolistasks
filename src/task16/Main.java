package task16;

import java.util.*;

public class Main {

    public static void revers(int[] arr) {
        int[] buf = arr.clone();
        for(int i = 0; i < arr.length; ++i)
            arr[i] = buf[arr.length - (i+1)];
    }

    public static void main(String[] args) {
        int[][] arr = new int[10][10];
        for(int a=0; a<10; ++a)
            for(int i=0; i<10; ++i)
                arr[a][i] = a*10+i+1;


        for(int i=0; i< 10;++i) {
            revers(arr[i]);
            System.out.println(Arrays.toString(arr[i]));
        }
    }
}