package task16.task161;

import java.util.*;


public class Main {

    public static void toLeft(int[][] arr) {
        for(int a=0; a < arr.length; ++a) {
            for (int i = 0; i < arr[a].length - 1; ++i)
                arr[a][i] = arr[a][i + 1];
            arr[a][arr[a].length-1] = 0;
        }
    }


    public static void main(String[] args) {
        int[][] arr = new int[10][10];
        for(int a=0; a<10; ++a)
            for(int i=0; i<10; ++i)
                arr[a][i] = a*10+i+1;

        toLeft(arr);

        for(int i=0; i< 10;++i)
            System.out.println(Arrays.toString(arr[i]));


    }
}