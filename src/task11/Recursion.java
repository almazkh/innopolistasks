package task11;

import java.io.File;
import java.io.IOException;

public class Recursion {
    public static void main(String[] args) {
        System.out.println("Обход всех файлов");
        new File("dir/a/b").mkdirs();
        try {
            new File("dir/1.txt").createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            new File("dir/2.txt").createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            new File("dir/a/b/2.txt").createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            new File("dir/a/b/3.txt").createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            new File("dir/a//b/4.txt").createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
list(new File("dir"));

    }

    static void list(File dir) {
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                System.out.println(file.getName());
                list(file);


            } else {
                System.out.println(file.getName());
            }
        }
    }
}