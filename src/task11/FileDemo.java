package task11;
import java.io.File;
import java.io.IOException;

public class FileDemo {
    public static void main(String[] args) {

        System.out.println(File.separator);
        File file = new File("test.txt");
        try {
            System.out.println();

            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Создание файла и каталога
        File dir = new File("Dir/a/b");
        System.out.println("Каталог?"+ dir.isDirectory());
        System.out.println("Файл?"+ dir.isFile());
        System.out.println("Создан каталог" + dir.mkdir());
        System.out.println("Каталог" + dir.isDirectory());
        System.out.println("Файл" + dir.isFile());

        //Переименование файла
        File newDir = new File("Dir/a/b");
        dir.renameTo(newDir);

        //удаление каталога
        System.out.println("Удаление файлов и каталогов");
        System.out.println("удаление" + file.getName() + ": " + file.delete());
        System.out.println("удаление" + dir.getName() + ": " + dir.delete());
    }

}