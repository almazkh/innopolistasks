package task19.FilterMap;


import java.util.*;

public class Main {
    public static boolean isUnique(Map<Integer, String> map){
        for (String s: map.values()) {
            int n = 0;
            for (String s_2 : map.values())
                if (s.compareTo(s_2) == 0)
                    ++n;
            if(n > 1)
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "One");
        map.put(2,"Two_");
        map.put(3,"Three");
        map.put(4, "Four");
        map.put(5, "Five");
        map.put(6,"Six");

        if(isUnique(map)) {
            System.out.println("true");
        }else
            System.out.println("false");

        Map<Integer, String> map_2 = new HashMap<>();
        map_2.put(1, "One");
        map_2.put(2,"Two_");
        map_2.put(3,"Three");
        map_2.put(4, "One");
        map_2.put(5, "Five");
        map_2.put(6,"Six");

        if(isUnique(map_2)) {
            System.out.println("true");
        }else
            System.out.println("false");

    }
}