package task19;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;

public class Main{

    public static void main (String args[]){
        Scanner in = new Scanner(System.in);
        Basket basket = new Products();
        int sel;
        String name;
        int quantity;
        List<String> products;
        while(true) {
            System.out.println("1.Добавить товар");
            System.out.println("2.Удалить товар");
            System.out.println("3.Изменить количество товара");
            System.out.println("4.Очистить корзину");
            System.out.println("5.Вывести список товаров");
            System.out.println("6.Получить кол-во товаров");

            while(!in.hasNextInt())
            {
                System.out.println("Не корректный ввод, попробуйте снова");
                in.nextLine();
            }

            sel = in.nextInt();

            in.nextLine();//холостой вызов для пропуска символа конца строки
            switch (sel) {
                case 1:
                    System.out.print("Введите название товара:");
                    name = in.nextLine();
                    System.out.print("Введите кол-во товара:");
                    while(!in.hasNextInt())
                    {
                        System.out.println("Не корректный ввод, попробуйте снова");
                        in.nextLine();
                    }
                    quantity = in.nextInt();
                    basket.addProduct(name, quantity);
                    break;
                case 2:
                    System.out.print("Введите название товара:");
                    name = in.nextLine();
                    basket.removeProduct(name);
                    break;
                case 3:
                    System.out.print("Введите название товара:");
                    name = in.nextLine();
                    System.out.print("Введите кол-во товара:");
                    quantity = in.nextInt();
                    basket.updateProductQuantity(name, quantity);
                    break;
                case 4:
                    basket.clear();
                    break;
                case 5:
                    products = (ArrayList<String>) basket.getProducts();
                    for(String product : products) {
                        System.out.println(product);
                    }
                    break;
                case 6:
                    System.out.print("Введите название товара:");
                    name = in.nextLine();
                    System.out.println(basket.getProductQuantity(name));
            }
        }
    }
}