package task19;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Products implements Basket {

    Map<String,Integer> pack =  new HashMap<String, Integer>();

    public void addProduct(String product, int quantity) {
        pack.put(product, quantity);
    }

    public void removeProduct(String product) {
        pack.remove(product);
    }

    public void updateProductQuantity(String product, int quantity) {
        if(pack.get(product) != null)
            pack.put(product, quantity);
    }

    public void clear() {
        pack.clear();
    }

    public List<String> getProducts() {
        List<String> ret = new ArrayList<String>(pack.keySet());
        return ret;
    }

    public int getProductQuantity(String product) {
        return pack.get(product);
    }
}

