package task18;

import java.util.*;

public class Main {
    public static Set removeEvenLength(Set<String> set){
        Set<String> result = new HashSet<>();
        for (String unit:
                set) {
            if(unit.length()%2 == 1)
                result.add(unit);
        }
        return result;
    }

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("One");
        set.add("Two_");
        set.add("Three");
        set.add("Four");
        set.add("Five");
        set.add("Six");

        Set<String> res = removeEvenLength(set);
        for (Iterator iter = set.iterator(); iter.hasNext();) {
            System.out.print(iter.next() + ", ");
        }

        for (Iterator iter = res.iterator(); iter.hasNext();) {
            System.out.print(iter.next() + ", ");
        }

    }
}