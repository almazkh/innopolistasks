package task15;

import org.jcp.xml.dsig.internal.SignerOutputStream;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class GetFromInternet {
    public static void main(String[] args) {
        try {
            URL url = new URL("https://api.coinlore.com/api/global/");
            try (InputStream is = url.openStream();
                 Reader reader = new InputStreamReader(is);
                 BufferedReader br = new BufferedReader(reader);


            ) {
                System.out.println(br.read());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
